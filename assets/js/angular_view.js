var myApp = angular.module('myApp',[])


myApp.controller('addnewcallCtrl', function($scope) {

 $scope.items = [];

  $scope.addRow = function(){
       $scope.items.push({sequence:'',sub_question:[]});
  }

   $scope.addQuestionRow = function(index){ 
       $scope.items[index]['sub_question'].push({sequence:''});
  }
 $scope.removeRow = function(index) {
    var retVal = confirm("Are you sure you want to delete?");
    if (retVal == true) {
      $scope.items.splice(index, 1);
    }
    }
 });
