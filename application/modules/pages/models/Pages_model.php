<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model {       
	function __construct(){            
    parent::__construct();
    $this->load->database();
		
	} 
	

	/**
      * This function is used to Insert Record in table
      * 
      */
	public function insert_addnewcall($data){
        if(!empty($data['question'])){
        	
	   	foreach ($data['question'] as $key => $value) {

	   		$insertdata['question'] = !empty($data['question'][$key])?$data['question'][$key]:'';
	   		$insertdata['question_type'] = !empty($data['question_type'][$key])?$data['question_type'][$key]:'';

            $this->db->insert('question', $insertdata);
	  	    $q_id = $this->db->insert_id();

		  	 
          if(!empty($data['answer_'.$key])){
			 foreach ($data['answer_'.$key] as $a_key => $v) {
		    	$insertdata_ans['answer'] = !empty($data['answer_'.$key][$a_key])?$data['answer_'.$key][$a_key]:'';
			    $insertdata_ans['q_id'] = $q_id;
			    $this->db->insert('answer', $insertdata_ans);
			 }
		  }
			  
          if(!empty($data['subquestion_'.$key])){ 

          	foreach ($data['subquestion_'.$key] as $k => $val) {

          	$insertsubdata['question'] = !empty($data['subquestion_'.$key][$k])?$data['subquestion_'.$key][$k]:'';
	   		$insertsubdata['question_type'] = !empty($data['subquestion_type_'.$key][$k])?$data['subquestion_type_'.$key][$k]:'';
          	$insertsubdata['p_id'] = $q_id;

            $this->db->insert('question', $insertsubdata);
	  	    $q_sub_id = $this->db->insert_id();
            
            if(!empty($data['subanswer_'.$key.$k])){
			   	foreach ($data['subanswer_'.$key.$k] as $a_key => $v) {
		   		$insertsubdata_ans['answer'] = !empty($data['subanswer_'.$key.$k][$a_key])?$data['subanswer_'.$key.$k][$a_key]:'';
		   		$insertsubdata_ans['q_id'] = $q_sub_id;
	            
	            $this->db->insert('answer', $insertsubdata_ans);
                }
              }  	   

          	}

          }

	   	}
	   }
	}

}?>