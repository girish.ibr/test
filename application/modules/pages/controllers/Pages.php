<?php defined("BASEPATH") OR exit("No direct script access allowed");
class Pages extends CI_Controller {
  	function __construct() {
	    parent::__construct();
	    $this->load->model("Pages_model"); 
		
		
  	}
	/**
	* This function is used to view page
	*/
	public function index() {   		
			
			$this->load->view("include/header");
			$this->load->view("index");
			$this->load->view("include/footer");			
		
	}

	/**
	* This function is used to ADD NEW CALL
	*/
		public function addnewcall(){
		 $datas =	$this->input->post();
		 $this->Pages_model->insert_addnewcall($datas);
		 redirect('/');
		}
	
}
?>