
<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper" ng-app="myApp">

  <header class="main-header"></header>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrappers" ng-controller="addnewcallCtrl">
     <form action="<?= base_url() ?>addnewcall" method="post">
      <!-- Form Element sizes -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title bColor">ADD NEW CALL</h3>
            </div>
            <div class="box box-success ht">
            <div class="box-body box" ng-repeat="item in items" ng-init="questionIndex=$index">
             <div class="row">
                 <div class="col-md-1" Style="width:0.2%">
                  <div class="form-group">                    
                     {{$index+1}}.
                  </div>
                </div>
                 <div class="col-md-7">
                  <div class="form-group">                    
                     <input type="text" ng-model="question" name="question[]" class="form-control">
	                  </div>
	               <div class="form-group" ng-if="question_type=='1'">
	                 <textarea ng-model="answer" name="answer_{{questionIndex}}[]" class="form-control"></textarea>             	
	               </div>	
	               <div class="form-group" ng-if="question_type=='2'">
	                 <input type="text" ng-model="answer" name="answer_{{questionIndex}}[]" class="form-control">  
	               </div>	
	               <div class="form-group" ng-if="question_type=='3'">
	                 <input type="text" ng-model="answer" name="answer_{{questionIndex}}[]" class="form-control">
	               </div>
	               <div class="form-group" ng-if="question_type=='3'">  
	                 <input type="text" ng-model="answer" name="answer_{{questionIndex}}[]" class="form-control">
	               </div>
	               <div class="form-group" ng-if="question_type=='3'">
	                 <input type="text" ng-model="answer" name="answer_{{questionIndex}}[]" class="form-control">
	               </div>
	               <div class="form-group" ng-if="question_type=='3'">  
	                 <input type="text" ng-model="answer" name="answer_{{questionIndex}}[]" class="form-control">
	               </div>
	               <div class="form-group" ng-if="question_type=='3'">
	                 <input type="text" ng-model="answer" name="answer_{{questionIndex}}[]" class="form-control">           	
	               </div>	
                </div>
              	<div class="col-md-3">
              		<div class="form-group">              			
              			<select name="question_type[]" ng-model="question_type" id="" class="form-control" >
              				<option value="">Select option</option>
              				<option value="1">Multiple-line text</option>
              				<option value="2">Single choice</option>
                      <option value="3">Multiple choice</option>
              			</select>
              		</div>               
              	</div>  
             
              </div> 
              <div class="row" ng-repeat="sub_question in item.sub_question" ng-init="subquestionIndex=$index">
                 <div class="col-md-1" Style="width:0.2%">
                  <div class="form-group">                    
                     {{questionIndex+1}}.{{subquestionIndex+1}}
                  </div>
                </div>
                 <div class="col-md-7">
                  <div class="form-group">                    
		                <input type="text" name="subquestion_{{questionIndex}}[]" class="form-control" ng-model="sub_question.question">		          	
                  </div>
                       <div class="form-group" ng-if="sub_question.question_type=='1'">
		                 <textarea ng-model="sub_question.answers" name="subanswer_{{questionIndex}}{{subquestionIndex}}[]" class="form-control"></textarea>             	
		               </div>	
		               <div class="form-group" ng-if="sub_question.question_type=='2'">
		                 <input type="text" ng-model="sub_question.answers" name="subanswer_{{questionIndex}}{{subquestionIndex}}[]" class="form-control">  
		               </div>	
		               <div class="form-group" ng-if="sub_question.question_type=='3'">
		                 <input type="text" ng-model="sub_question.answers" name="subanswer_{{questionIndex}}{{subquestionIndex}}[]" class="form-control">
		               </div>
		               <div class="form-group" ng-if="sub_question.question_type=='3'">  
		                 <input type="text" ng-model="sub_question.answerss" name="subanswer_{{questionIndex}}{{subquestionIndex}}[]" class="form-control">
		               </div>
		               <div class="form-group" ng-if="sub_question.question_type=='3'">
		                 <input type="text" ng-model="sub_question.answersss" name="subanswer_{{questionIndex}}{{subquestionIndex}}[]" class="form-control">
		               </div>
		               <div class="form-group" ng-if="sub_question.question_type=='3'">  
		                 <input type="text" ng-model="sub_question.answerssss" name="subanswer_{{questionIndex}}{{subquestionIndex}}[]" class="form-control">
		               </div>
		               <div class="form-group" ng-if="sub_question.question_type=='3'">
		                 <input type="text" ng-model="sub_question.answess" name="subanswer_{{questionIndex}}{{subquestionIndex}}[]" class="form-control">           	
		               </div>
                </div>
              	<div class="col-md-3">
              		<div class="form-group">              			
              			<select name="subquestion_type_{{questionIndex}}[]" id="" class="form-control" ng-model="sub_question.question_type">
              				<option value="">Select option</option>
              				<option value="1">Multiple-line text</option>
              				<option value="2">Single choice</option>
                      <option value="3">Multiple choice</option>
              			</select>
              		</div>               
              	</div>
        
              </div> 
              
                 <div class="col-md-2">
                  <div class="form-group">                    
                    <a href="javascript:void(0)" ng-click="addQuestionRow(questionIndex)"> + Add Sub Question </a>
                  </div>
                </div>             
            </div>
           </div> 
             <div class="box-header with-border box-feature">
              <a href="javascript:void(0)" ng-click="addRow()"> + Add New Question </a>
            </div>
            <!-- /.box-body -->
          <div class="row mt-15"> 
             <div class="col-md-6">
                  <div class="form-group">  
              <button class="btn btn-primary">Save</button> <button class="btn" type="reset">Cancel</button>
                  </div>
              </div>
          </div>
          </div>
          <!-- /.box -->
        </form>  
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


</body>
</html>
<style type="text/css">
.box-feature{
  text-align: right;
  border: 1px solid #f4f4f4;
  background-color: #f4f4f4;
}
.mt-15{
  margin-top:15px;
}

.skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side {
  background-color: #fff!important; 
}
.ht{
      height: 450px;	
      overflow-x: auto;
      padding: 5px;
  }
 .bColor{
 	color: #3c8dbc;
 } 

 .box{
 	    border-top: 0px solid #d2d6de;
 }
</style>